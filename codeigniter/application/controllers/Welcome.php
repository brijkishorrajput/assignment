<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//phpinfo();
		$this->mdb->select_db('brij');
		if($this->mdb->insert('fruits',array("name"=>"banana1", "amount"=>50, "color"=>"yellow"))){
			echo "Inserted a new fruit with id: $this->mdb->insert_id()";
		}else{
			echo "Could not be inserted";
		}
		$this->load->view('welcome_message');
	}
}
