<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Stories extends REST_Controller {
    
	
    public function __construct() {
       parent::__construct();
       
    }
       
    /**
     * Get All Data from stories collection index_get method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        
            
        if(!empty($id)){
            $this->mdb->where(array("_id"=> new MongoDB\BSON\ObjectId($id))); 
            $data = $this->mdb->get('story')->result();        
        }else{
            $data = $this->mdb->get('story')->result(); 
          
        }

        if(count($data)){
            $this->response($data, REST_Controller::HTTP_OK);
    
        }else{           
            $this->response(['No Record!'], REST_Controller::HTTP_OK);
        
        }
     
       
	}
      
    /**
     * Insert Data from index_post method.
     *
     * @return Response
    */
    public function index_post()
    {
        $input = $this->input->post();
       // print_r($this->input->post());
        if($this->mdb->insert('story',$input)){
            $this->response(['Story created successfully.'], REST_Controller::HTTP_OK);
        }else{
            $this->response(['Could not be inserted'], REST_Controller::HTTP_OK);
        }
        
        
    } 
     
    /**
     * Update Data from index_put method.
     *
     * @return Response
    */
    public function update_post()
    {
        $input = $this->post();
        
        unset($input["_id"]);
        $this->mdb->update("story",$input,array('_id'=>new MongoDB\BSON\ObjectId($this->post("_id"))));
     
        $this->response(['Story updated successfully.'], REST_Controller::HTTP_OK);
    }
     
    /**
     * Delete Data from index_delete method.
     *
     * @return Response
    */
    public function index_delete($id)
    {
        $this->mdb->delete("story",array("_id"=>new MongoDB\BSON\ObjectId($id)),$limit=0);
        
        $this->response([' Story deleted successfully.'], REST_Controller::HTTP_OK);
    }
        
    
    /**
     * Insert like  Data from like_post method.
     *
     * @return Response
    */
    public function like_post()
    {
        $input = $this->input->post();
        // check data exists or not 

        $this->mdb->where(array(
            "story_id"=> $input['story_id'],
            "user_id"=> $input['user_id']
        )); 
        $data = $this->mdb->get('story_like')->result();

        if(count($data)){
            $this->response(['You Already Vote for this Story.'], REST_Controller::HTTP_OK);
    
        }else{
            $this->mdb->insert('story_like',$input);
        
            $this->response(['You have vote successfully for this Story.'], REST_Controller::HTTP_OK);
    
    
        }

        } 
}