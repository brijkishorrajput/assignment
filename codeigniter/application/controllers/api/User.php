<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class User extends REST_Controller {
    
	
    public function __construct() {
       parent::__construct();
       
    }
       
    /**
     * Get All Data from stories collection index_get method.
     *
     * @return Response
    */
	public function login_post()
	{
        
        $input = $this->input->post();
        
        $this->mdb->where(array(
                            "email"=> $input['email'],
                            "password"=> $input['password'],
                            "is_active"=>1
                        )); 
        $data = $this->mdb->get('user')->result();        
        
        if(count($data)){
            $this->response($data, REST_Controller::HTTP_OK);
        }else{
            $this->response(['No Record Match.'], REST_Controller::HTTP_OK);
    
        }
	}
      
   
}