import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'http://localhost/assignment/codeigniter/public/api/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getStories(): Observable<any> {
    return this.http.get(endpoint + 'stories').pipe(
      map(this.extractData));
  }
  
  getStory(id): Observable<any> {
    return this.http.get(endpoint + 'stories/' + id).pipe(
      map(this.extractData));
  }
  
  deleteStory (id): Observable<any> {
    return this.http.delete<any>(endpoint + 'stories/' + id, httpOptions).pipe(
      tap(_ => console.log(`deleted story id=${id}`)),
      catchError(this.handleError<any>('deleteStory'))
    );
  }

  upvoteStory (upvote): Observable<any> {
    return this.http.post<any>(endpoint + 'stories/like', JSON.stringify(upvote), httpOptions).pipe(
      tap((upvote) => console.log(`upvote story w/ id=${upvote.story_id}`)),
      catchError(this.handleError<any>('upvoteStory'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      console.error(error); 
  
      console.log(`${operation} failed: ${error.message}`);
  
      return of(result as T);
    };
  }

}
