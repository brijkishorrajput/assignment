import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoryComponent } from './story/story.component';
import { StoryDetailComponent } from './story-detail/story-detail.component';

const appRoutes: Routes = [
  {
    path: 'story',
    component: StoryComponent,
    data: { title: 'Story List' }
  },
  {
    path: 'story-details/:id',
    component: StoryDetailComponent,
    data: { title: 'Story Details' }
  }
];

@NgModule({
  declarations: [
    AppComponent,
    StoryComponent,
    StoryDetailComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
