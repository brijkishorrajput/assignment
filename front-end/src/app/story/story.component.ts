import { Component, OnInit,Input } from '@angular/core';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {

  stories:any = [];
  @Input() productData = { user_id:'', stry_id: ''};

  constructor(public rest:RestService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getStories();
  }

  getStories() {
    this.stories = [];
    this.rest.getStories().subscribe((data: {}) => {
      console.log(data);
      this.stories = data;
    });
  }

  delete(id) {
    this.rest.deleteStory(id)
      .subscribe(res => {
          this.getStories();
        }, (err) => {
          console.log(err);
        }
      );
  }

  upvote() {
    this.rest.upvoteStory(this.productData)
      .subscribe(res => {
          this.getStories();
        }, (err) => {
          console.log(err);
        }
      );
  }

}